# README #
## Purpose: Create a dictionary for organs and species that groups values together based on word similarity

##Procedure:

1) Get data from site in json format and store unique values in list

2) Create a dictionary for organs or species that groups synonyms together, checks for plural versions of words for 's' and 'ves' when matching 

3) For species the dictionary is modified by searching through umatched values for shared family names

4) If terms are similar then the longer string term and its synonyms get added as a synonym to the shorter string term and the longer one is removed from dict, creating a nested dictionary

5) Unmatched values are added back into dictionary

6) List of (parent,child) tuples is made from dictionary

7) Tuple list is used to create nodes on given namespace

##Issues:

 
1) Can't differentiate different meanings of the same word, eg. stem from plants versus stem cells 

2) Some values match to several keys and cannot figure out which key the value is the best fit for, such as "cell pellet and culture media" is in key: media and also cell pellet subgroup in key: cells and adipose tissue is in both Key: tissue and key: adipose (Somewhat fixed because value is only matched once)

3)~~ Can't differentiate between plural and singular forms, eg. cell and cells, treated as separate things~~(FIXED)

4) Can't match if words are the same but structure is not, eg. "leaves and roots" and "roots and leaves"

5)~~ Dictionary only includes terms that matched with others, ones with no matches are not in it~~(FIXED)

6) For species if there was already a match when creating first creating the dictionary when searching by family name it will not be included


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact