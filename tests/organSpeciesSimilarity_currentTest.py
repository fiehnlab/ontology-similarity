import unittest
import logging
import os
import sys
import collections
import json

from organ_species_similarity import group_by_similarity, nested_dictionary, merge_unmatched, species_match, create_list

class TestOrganSimilarity(unittest.TestCase):
    def test_dictionary1(self): # test 1 checks that values only match once and in descending alphabetical and whitespace order
        test_input = ['muscle', 'muscle tissue', 'heart muscle', 'heart tissue', 'blood tissue', 'blood', 'tissue','liver tissue', 'liver tissue cell']
        #test_input.sort(key = lambda x: (x.count(' '), x) )
        group, l = group_by_similarity(test_input)
        nested_dictionary(group)
        expected_result = open('test1.json', 'rb')
        expected_output = expected_result.read().rstrip()
        test_output = json.dumps(group, sort_keys = True, indent = 4)
        self.assertEqual(test_output, expected_output)

    def test_dictionary2_1(self): # test 2 part 1 checks that plural versions of words match and direct plural version of key is deleted
        test_input = ['hoof', 'hooves', 'calf', 'calves', 'small calf', 'large calves', 'cell', 'cells', 'blood cells', 'red blood cells', 'blood cell', 'stems', 'plant stem']
        #test_input.sort(key = lambda x: (x.count(' '), x) )
        group, l = group_by_similarity(test_input)
        expected_result = open('test2_part1.json', 'rb')
        expected_output = expected_result.read().rstrip()
        test_output = json.dumps(group, sort_keys = True, indent = 4)
        self.assertEqual(test_output,expected_output)

    def test_dictionary2_2(self): # test 2 part 2 checks that the nested dictionary is created correctly
        test_input = ['hoof', 'hooves', 'calf', 'calves', 'small calf', 'large calves', 'cell', 'cells', 'blood cells', 'red blood cells', 'blood cell', 'stems', 'plant stem']
        #test_input.sort(key = lambda x: (x.count(' '), x) )
        group, l = group_by_similarity(test_input)
        nested_dictionary(group)
        expected_result = open('test2_part2.json', 'rb')
        expected_output = expected_result.read().rstrip()
        test_output = json.dumps(group, sort_keys = True, indent = 4)
        self.assertEqual(test_output,expected_output)

    def test_species(self): # test that species are sorted correctly
        test_input = ['e. coli', 'e. coli sample', 'felidae cat', 'felidae cougar', 'canine', 'canine wolf', 'canine dog', 'elephant']
        grouped_species = collections.defaultdict(list)
        group, l = species_match(grouped_species, test_input)
        test_output = json.dumps(grouped_species, sort_keys = True, indent = 4)
        expected_result = open('test3.json', 'rb')
        expected_output = expected_result.read().rstrip()
        self.assertEqual(test_output,expected_output)

    def test_merge(self): # tests that unmatched items that are not exact plurals of singlular word are added back into dictionary
        test_input = ['monkey', 'monkeys', 'sick monkeys', 'ape', 'fish', 'salmon', 'sockeye salmon', 'leaf', 'dead leaves', 'cells', 'heart cell', 'liver']
        group, l = group_by_similarity(test_input)
        merge_unmatched(group, l)
        test_output = json.dumps(group, sort_keys = True, indent = 4)
        expected_result = open('test4.json', 'rb')
        expected_output = expected_result.read().rstrip()
        self.assertEqual(test_output,expected_output)


    def test_alltogether(self): # tests to check if dictionary in its entirety is built correctly
        test_input = ['a. thaliana', 'a. thaliana leaves', 'a. thaliana leaves treatment1', 'cell', 'cells', 'lung cells', 'damaged lung cells', 'liver cell', 'kidney', 'bacteria', 'calf', 'small calves', 'dogs', 'large dog', 'family species1', 'family species2', 'family species1 brain tissue', 's. cerevisiae sample', 's. cerevisiae culture']
        group, l = group_by_similarity(test_input)
        dictionary, nm = species_match(group, l)
        nested_dictionary(dictionary)
        merge_unmatched(dictionary, nm)
        test_output = json.dumps(dictionary, sort_keys = True, indent = 4)
        expected_result = open('test5.json', 'rb')
        expected_output = expected_result.read().rstrip()
        self.assertEqual(test_output,expected_output)

    def test_parentChild(self): # tests that a list of parent-child tuples are created given a dictionary
        test_input = ['hoof', 'hooves', 'calf', 'calves', 'small calf', 'large calves', 'cell', 'cells', 'blood cells', 'red blood cells', 'blood cell', 'stems', 'plant stem']
        group, l = group_by_similarity(test_input)
        nested_dictionary(group)
        merge_unmatched(group, l)
        pc_list = create_list(group, 'organ')
        test_output = json.dumps(pc_list)
        expected_result = open('test6.json', 'rb')
        expected_output = expected_result.read().rstrip()
        self.assertEqual(test_output,expected_output)



if __name__ == '__main__':
    unittest.main()
