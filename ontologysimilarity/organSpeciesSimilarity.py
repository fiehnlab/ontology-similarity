# Name: organSpeciesSimilarity.py by Daria Afonskaia
# Purpose: To group organs together that share similarities in a dictionary as synonyms and then push parent and children to rest database
# Data created: 6/26/2017
# Acknowledgements: Sajjan Singh Mehta, Gert Wohlgemuth

from __future__ import print_function


import collections
import json
import requests
import re
import time


PUSH_URL = 'http://dict.fiehnlab.ucdavis.edu'


class ontology_service(object):
    '''
        Creates a nested dictionary for data given and then pushes the parent-child groupings to server
    '''

    def __init__(self, rest_site):
        self.data = requests.get(rest_site)
        self.data = self.data.json()
        self.data_list = list(set(x['name'].lower().strip() for x in self.data))
        #self.dic, self.unmatched = group_by_similarity(self.data_list)

        '''self.new_dic, no_match = species_match(dic, leftovers)
        nested_dictionary(self.new_dic) # creates nested dictionary
        merge_unmatched(self.new_dic, no_match)
        self.tree = create_list(self.new_dic, group_type)'''

    '''
    Name: group_by_similarity
    Purpose: To create a dictionary using word similarity
    Parameters: none
    Return: dictionary, list
    '''
    def group_by_similarity(self):
        items = self.data_list
        delete_keys = [] # create list to store macthed keys that need to be removed from dictionary
        self.unmatched = [] # create list to store unmatched values to add back to dictionary later
        groups = collections.defaultdict(list) # create a dictionary to store the key oragans and their values
        items.sort(key = lambda x: (x.count(' '), x) ) #sort the list by whitespace
        for i, mapList in enumerate(items):
            for mapList2 in items[i + 1 :]:
                if mapList2 in items:
                    searchList = re.search(r'\b'+ mapList + r'\b', mapList2) # check if words match
                    if searchList:
                        delete_keys.append(mapList2) # value matches a key then it is deleted from being a key in dictionary
                        if mapList in groups: # if key already exists then add new match value
                            if mapList2 in groups[mapList]:
                                continue
                            groups[mapList].append(mapList2)
                        else: # if key does not exist make a new key and assign matched value
                            groups[mapList] = [mapList2]
                        items.remove(mapList2) #removes match to prevent values to be matched to multiple keys

                    elif not searchList: # if the words don't match as they are, check if there are plural forms that match to singlular forms
                        if mapList.endswith('s'): # if the key ends in s then get rid of it
                            mod_mapList1 = mapList[:-1]
                        else:
                            mod_mapList1 = mapList + 's' # if key does not end in s then add it

                        searchList2 = re.search(r'\b'+ mod_mapList1 + r'\b', mapList2) # check if value matches plural or singlular version of key
                        if searchList2:
                            delete_keys.append(mapList2)
                            # check_plural = re.search(mapList2, mod_mapList1) # value is the exact plural form of the key then it is deleted
                            # if check_plural:
                            #     items.remove(mapList2)
                            #     continue
                            if mapList in groups: # if key already exists then add new match value
                               groups[mapList].append(mapList2)
                            else: # if key does not exist make a new key and assign matched value
                                groups[mapList] = [mapList2]
                            items.remove(mapList2)

                        elif not searchList2:
                            searchItem = re.search('ves', mapList2) # check if value contains 'ves'
                            if searchItem: # if it does then key is modified to end with 'ves'
                                if mapList.endswith('f'):
                                    mod_maplist = mapList[:-1]
                                    mod_maplist = mod_maplist + 'ves'
                                    searchList3 = re.search(r'\b'+ mod_maplist + r'\b', mapList2)
                                    if searchList3:
                                        delete_keys.append(mapList2)
                                        if mapList in groups:
                                            groups[mapList].append(mapList2)
                                        else:
                                            groups[mapList] = [mapList2]
                                        items.remove(mapList2)


        all_grouped_items = sum([[key] + value for key, value in groups.items()], []) # stores values for items in dictionary

        for item in items: # checks if which values did not have matches(did not make it into the dictionary)
                if item not in all_grouped_items:
                        self.unmatched.append(item)

        delete_keys = list(set(delete_keys)) # gets rid of duplicates
        for key in delete_keys: # deletes the keys that matched to other keys and were added as values
            for group in list(groups.keys()):
                searchKey = re.search(key, group)
                if searchKey:
                    del groups[group]

        self.dictionary = groups
        return self.dictionary, self.unmatched # return the dictionary, and unmatched values

    '''
        Name: species_match
        Purpose: go through unmatched values and check species are in the same family
        Parameters: boolean
        Return: none
    '''
    def species_match(self, check_species = True):
        species = self.unmatched
        self.no_matches = [] # create list to store unmatched values to return
        species.sort(key = lambda x: (x.count(' '), len(x)) ) # sort list by whitespace and by length
        for i, mapList in enumerate(species):
            mapList = mapList.encode('utf-8') # remove unicode errors
            species_name = mapList.split()[0] # get first word from string
            for mapList2 in species[i + 1 :]:
                mapList2 = mapList2.encode('utf-8')
                if species_name.endswith('.'): # useful if group_by_similarity is not run
                    if check_species:
                        name = mapList.split()
                        species_name = name[0] + ' ' + name [1] # gets first and second word from string if first word ends with '.'
                        Search_species = re.search(r'\b'+ species_name.replace('.', '\\.') , mapList2)
                else:
                    Search_species = re.search(r'\b'+ species_name + r'\b' , mapList2)
                if Search_species:
                    if species_name in self.dictionary:
                        if mapList2 in self.dictionary[species_name]:
                            species.remove(mapList2)
                            continue
                        self.dictionary[species_name].append(mapList2)
                    else:
                        if species_name != mapList: # if the species name and string are not the same then both values get added
                            self.dictionary[species_name] = [mapList]
                            self.dictionary[species_name].append(mapList2)
                        else:
                            self.dictionary[species_name] = [mapList2] # otherwise only the matched value gets added
                    species.remove(mapList2) # remove matched value from list


        all_grouped_items = sum([[key] + value for key, value in self.dictionary.items()], []) # stores values for items in dictionary

        for item in species: # checks if which values did not have matches(did not make it into the dictionary)
                if item not in all_grouped_items:
                        self.no_matches.append(item)


    '''
        Name: merge_unmatched
        Purpose: add unmatched values as keys in dictionary
        Parameters: none
        Return: none
    '''
    def merge_unmatched(self):
        for l in self.no_matches:
            self.dictionary[l] = [] # create key for each unmatched value

    '''
        Name: nested_dictionary
        Purpose: to check each key for values that match with each other and create a nested dictionary
        Parameters: none
        Return: none
    '''
    def nested_dictionary(self):
        dictionary= self.dictionary
        for k in dictionary:
            self.data_list = dictionary[k]
            dictionary[k], singlematch = self.group_by_similarity() # make a nested dictionary, if values match other values then a sub key is created and the matched value is transferred there
            for s in singlematch:
                dictionary[k][s] = [] # add back the values that did not match to other values in the key
        self.dictionary = dictionary

    '''
        Name: create_list
        Purpose: to create a list of parent and child tuples from dictionary
        Parameters: string
        Return: none

    '''
    def create_list(self, root):
        tree_list = [] # creates list to store tuples

        for k,v in self.dictionary.items(): # goes through key and value pairs
            tree_list.append((root, k)) # adds root and key as a tuple

            if isinstance(v, dict): # if the value is a dictionary then it goes through its key value pairs and adds them as tuples
                if len(v) > 0:
                    for key, values in v.items():
                        tree_list.append((k, key)) # the key from the values dictionary gets added as a childen of the original dictionary key

                        for val in values:
                            tree_list.append((key, val))
        self.root = root
        self.tree_list = tree_list

    '''
        Name: server_push
        Purpose: to push parent-child tuples to server
        Parameters: none
        Return: none

    '''
    def server_push(self):
        self.counter = 0
        for k in self.tree_list:
            value = k[1].replace('/', 'forwardslash')
            value = value.encode('utf-8')

            print("\tChecking %s:%s..." % (self.root, value), end = '')
            check = requests.get(PUSH_URL +"/rest/ontology/node/exist/"+self.root+":"+ value).text # check if node exists in namespace

            if check == 'false':
                requests.put(PUSH_URL +"/rest/ontology/node/"+ self.root +":"+ value + "/isChildOf/"+ self.root +":" + k[0]) # if it doesn't exist node gets created of parent and child
                print()
                print('\t\t%s pushed as child of %s' % (self.root +":"+ value, self.root +":" + k[0]))
                self.counter += 1
            else:
                print('exists!')

def run():
    print('Starting synchonization at %s...' % time.strftime("%Y-%m-%d %H:%M"))

    species = ontology_service('http://minix.fiehnlab.ucdavis.edu/rest/getSpecies')
    species.group_by_similarity()
    species.species_match()
    species.nested_dictionary()
    species.merge_unmatched()
    species.create_list('species')
    species.server_push()
    print('Entries added for species: ', species.counter)

    organ = ontology_service('http://minix.fiehnlab.ucdavis.edu/rest/getOrgans')
    organ.group_by_similarity()
    organ.species_match(False)
    organ.nested_dictionary()
    organ.merge_unmatched()
    organ.create_list('organ')
    organ.server_push()
    print('Entries added for organs: ', organ.counter)

    print('Finished synchonization at %s!' % time.strftime("%Y-%m-%d %H:%M"))


if __name__ == '__main__':
    run()
