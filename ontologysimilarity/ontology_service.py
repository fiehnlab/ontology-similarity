from __future__ import print_function

import organSpeciesSimilarity
import collections
import json
import requests
import re
import threading
import bottle

from bottle import route, run, template


# creates species and organ dictionaries and pushes items that are not in the database
def run_ontologyService():
    try:
        organSpeciesSimilarity.run()
    except Exception as e:
        print('Synchronization failed at %s:' % time.strftime("%Y-%m-%d %H:%M"))
        print(e)

    # reschedule this function to run again in an hour
    threading.Timer(3600, run_ontologyService).start()


# runs the ontology service if url is accessed
@route('/rest/schedule')
def rest_call():
    return bottle.HTTPResponse(status=200)

if __name__ == '__main__':
    # Run the background service every hour
    run_ontologyService()

    # Run the web server to invoke service - disabled
    # run(host='localhost', port=8080)
