#!/bin/bash

# Name of the docker image
IMAGE_NAME="ontology-synchronization"
IMAGE_REGISTRY="eros.fiehnlab.ucdavis.edu/$IMAGE_NAME"

# Build docker image
docker build -f docker/Dockerfile -t $IMAGE_NAME . | tee docker_build.log || exit 1

# Tag the docker container
ID=$(tail -1 docker_build.log | awk '{print $3;}')
docker tag $ID $IMAGE_NAME:latest
docker tag $ID $IMAGE_REGISTRY:latest

echo "Tagged $ID as $IMAGE_NAME:latest"
echo "Tagged $ID as $IMAGE_REGISTRY:latest"

rm docker_build.log

# Push if requested
if [ "$1" == "push" ]; then
	echo "pushing $IMAGE to server"
	docker push $IMAGE_REGISTRY
else
	echo "pushing disabled - use 'push' as argument to push to the docker registry"
fi